#include <iostream>
#include <string>
using namespace std;

int** prepareMatrix(int p_height, int p_width) {
	int** result = new int*[p_height];

	for (int i = 0; i < p_height; i++) {
		result[i] = new int[p_width];
		result[i][0] = 0;
	}

	for (int i = 1; i < p_width; i++) {
		result[0][i] = 0;
	}

	return result;
}

void deleteMatrix(int** p_matrix, int p_height) {
	for (int i = 0; i < p_height; i++) {
		delete[] p_matrix[i];
	}
	
	delete[] p_matrix;
}

int max(int p_first, int p_second) {
	return (p_first > p_second) ? p_first : p_second;
}


int main() {
	ios_base::sync_with_stdio(false);

	int z;
	cin >> z;

	while (z--) {
		string firstString, secondString;
		cin >> firstString >> secondString;

		int** longestCommonSubsequence = prepareMatrix(firstString.length() + 1,
													   secondString.length() + 1);

		for (int first = 1; first <= firstString.length(); first++) {
			for (int second = 1; second <= secondString.length(); second++) {

				if (firstString[first - 1] == secondString[second - 1]) {
					longestCommonSubsequence[first][second] = 1 + longestCommonSubsequence[first - 1][second - 1];
				} else {
					longestCommonSubsequence[first][second] = max(longestCommonSubsequence[first - 1][second],
																  longestCommonSubsequence[first][second - 1]);
				}
			}
		}

		cout << longestCommonSubsequence[firstString.length()][secondString.length()] << endl;

		string commonSubsequence;
		int first = firstString.length();
		int second = secondString.length();

		while (longestCommonSubsequence[first][second] > 0) {
			if (firstString[first - 1] == secondString[second - 1]) {
				first--;
				second--;
				commonSubsequence.push_back(firstString[first]);
			} else if(longestCommonSubsequence[first - 1][second] > longestCommonSubsequence[first][second - 1]) {
				first--;
			} else {
				second--;
			}
		}

		for (int i = commonSubsequence.length() - 1; i >= 0; i--) {
			cout << commonSubsequence[i];
		}
		cout << endl;

		deleteMatrix(longestCommonSubsequence, firstString.length() + 1);
	}
}