/**
 * Created by Pawel Polit
 */

#include <iostream>

using namespace std;


struct node {
    int barrel;
    int numberOfNodesInSubtree;
    bool invertedOrderInSubtree;
    node* left;
    node* right;
    node* parent;


    node(int p_barrel, int p_numberOfNodesInSubtree, node* p_parent) {
        barrel = p_barrel;
        numberOfNodesInSubtree = p_numberOfNodesInSubtree;
        invertedOrderInSubtree = false;
        left = NULL;
        right = NULL;
        parent = p_parent;
    }

    bool hasIndex(int p_index) {
        return numberOfNodesInSubtree == p_index;
    }

    bool hasIndexGreaterThen(int p_index) {
        return numberOfNodesInSubtree > p_index;
    }

    void invertOrder() {
        invertedOrderInSubtree = !invertedOrderInSubtree;
    }

    void fixOrder() {
        if(invertedOrderInSubtree) {
            invertedOrderInSubtree = false;

            node* temporary = right;
            right = left;
            left = temporary;

            if(left != NULL) {
                left->invertOrder();
            }

            if(right != NULL) {
                right->invertOrder();
            }
        }
    }
};


class tree {
    node* root;
    int size;

    void rightRotation(node* p_node) {
        int forCurrent = 0, forParent = 0;

        if(p_node->parent->right != NULL) {
            forCurrent = p_node->parent->right->numberOfNodesInSubtree;
        }

        if(p_node->left != NULL) {
            forParent = p_node->left->numberOfNodesInSubtree;
        }

        p_node->numberOfNodesInSubtree += 1 + forCurrent;
        p_node->parent->numberOfNodesInSubtree += -1 - forParent;

        if(p_node->parent->parent != NULL) {
            if(p_node->parent->parent->left == p_node->parent) {
                p_node->parent->parent->left = p_node;
            } else {
                p_node->parent->parent->right = p_node;
            }
        }

        p_node->parent->left = p_node->right;
        if(p_node->right != NULL) {
            p_node->right->parent = p_node->parent;
        }
        p_node->right = p_node->parent;
        p_node->parent = p_node->parent->parent;
        p_node->right->parent = p_node;

        if(p_node->parent == NULL) {
            root = p_node;
        }
    }

    void leftRotation(node* p_node) {
        int forCurrent = 0, forParent = 0;

        if(p_node->parent->left != NULL) {
            forCurrent = p_node->parent->left->numberOfNodesInSubtree;
        }

        if(p_node->right != NULL) {
            forParent = p_node->right->numberOfNodesInSubtree;
        }

        p_node->numberOfNodesInSubtree += 1 + forCurrent;
        p_node->parent->numberOfNodesInSubtree += -1 - forParent;

        if(p_node->parent->parent != NULL) {
            if(p_node->parent->parent->left == p_node->parent) {
                p_node->parent->parent->left = p_node;
            } else {
                p_node->parent->parent->right = p_node;
            }
        }

        p_node->parent->right = p_node->left;
        if(p_node->left != NULL) {
            p_node->left->parent = p_node->parent;
        }
        p_node->left = p_node->parent;
        p_node->parent = p_node->parent->parent;
        p_node->left->parent = p_node;

        if(p_node->parent == NULL) {
            root = p_node;
        }
    }

    void splay(node* p_node) {

        while(root != p_node) {
            if(p_node->parent == root) {
                if(root->left == p_node) {
                    rightRotation(p_node);
                } else {
                    leftRotation(p_node);
                }
            } else if(p_node->parent->left == p_node && p_node->parent->parent->left == p_node->parent) {
                rightRotation(p_node->parent);
                rightRotation(p_node);
            } else if(p_node->parent->right == p_node && p_node->parent->parent->right == p_node->parent) {
                leftRotation(p_node->parent);
                leftRotation(p_node);
            } else if(p_node->parent->right == p_node && p_node->parent->parent->left == p_node->parent) {
                leftRotation(p_node);
                rightRotation(p_node);
            } else {
                rightRotation(p_node);
                leftRotation(p_node);
            }
        }
    }

    node* findNthBarrel(int p_index, node* p_node) {
        p_node->fixOrder();

        if(p_node->left == NULL) {
            if(p_index == 1) {
                return p_node;
            } else {
                return findNthBarrel(p_index - 1, p_node->right);
            }
        }

        if(p_node->left->hasIndex(p_index - 1)) {
            return p_node;
        }

        if(p_node->left->hasIndexGreaterThen(p_index - 1)) {
            return findNthBarrel(p_index, p_node->left);
        }

        return findNthBarrel(p_index - p_node->left->numberOfNodesInSubtree - 1, p_node->right);
    }

    void invertPrefix(int p_end) {
        if(p_end == size) {
            root->invertOrder();
            return;
        }

        node* newRoot = findNthBarrel(p_end + 1, root);

        if(newRoot != root) {
            splay(newRoot);
        }
        if(root->left != NULL) {
            root->left->invertOrder();
        }
    }

    void removeSubtree(node* p_node) {
        if(p_node->left != NULL) {
            removeSubtree(p_node->left);
        }

        if(p_node->right != NULL) {
            removeSubtree(p_node->right);
        }

        delete p_node;
    }

public:

    tree(int p_size) {
        size = p_size;
        root = new node(1, p_size, NULL);
        node* current = root;

        for(int barrel = 2; barrel <= p_size; ++barrel) {
            current->right = new node(barrel, p_size - barrel + 1, current);
            current = current->right;
        }
    }

    ~tree() {
        removeSubtree(root);
    }

    void invertInterval(int p_begin, int p_end) {
        cout << findNthBarrel(p_begin, root)->barrel << ' ' << findNthBarrel(p_end, root)->barrel << '\n';

        if(p_begin != p_end) {
            invertPrefix(p_end);
            invertPrefix(p_end - p_begin + 1);
            invertPrefix(p_end);
        }
    };
};


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfBarrels, numberOfOperations;
        cin >> numberOfBarrels >> numberOfOperations;

        tree barrelTree(numberOfBarrels);

        while(numberOfOperations--) {
            int begin, end;
            cin >> begin >> end;

            barrelTree.invertInterval(begin, end);
        }
    }
}