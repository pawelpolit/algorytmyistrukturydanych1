/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <limits>

using namespace std;


struct node {
    int beginFirst = numeric_limits<int>::max(), beginSecond = numeric_limits<int>::max();
    int endFirst = numeric_limits<int>::max(), endSecond = numeric_limits<int>::max();
    bool firstFirst = true, firstSecond = true, secondFirst = true, secondSecond = true;
};

class intervalTree {
    vector<node> vectorRepresentation;

    int left(int p_nodeIndex) {
        return p_nodeIndex << 1;
    }

    int right(int p_nodeIndex) {
        return (p_nodeIndex << 1) + 1;
    }

    int parent(int p_nodeIndex) {
        return p_nodeIndex >> 1;
    }

    void updateNode(int p_nodeIndex) {
        node& leftChild = vectorRepresentation[left(p_nodeIndex)];
        node& rightChild = vectorRepresentation[right(p_nodeIndex)];

        vectorRepresentation[p_nodeIndex].beginFirst = leftChild.beginFirst;
        vectorRepresentation[p_nodeIndex].beginSecond = leftChild.beginSecond;
        vectorRepresentation[p_nodeIndex].endFirst = rightChild.endFirst;
        vectorRepresentation[p_nodeIndex].endSecond = rightChild.endSecond;

        vectorRepresentation[p_nodeIndex].firstFirst = leftChild.beginFirst <= rightChild.endFirst &&
                                                       ((leftChild.endFirst <= rightChild.beginFirst &&
                                                         leftChild.firstFirst && rightChild.firstFirst) ||
                                                        (leftChild.endFirst <= rightChild.beginSecond &&
                                                         leftChild.firstFirst && rightChild.secondFirst) ||
                                                        (leftChild.endSecond <= rightChild.beginFirst &&
                                                         leftChild.firstSecond && rightChild.firstFirst) ||
                                                        (leftChild.endSecond <= rightChild.beginSecond &&
                                                         leftChild.firstSecond && rightChild.secondFirst));

        vectorRepresentation[p_nodeIndex].firstSecond = leftChild.beginFirst <= rightChild.endSecond &&
                                                        ((leftChild.endFirst <= rightChild.beginFirst &&
                                                          leftChild.firstFirst && rightChild.firstSecond) ||
                                                         (leftChild.endFirst <= rightChild.beginSecond &&
                                                          leftChild.firstFirst && rightChild.secondSecond) ||
                                                         (leftChild.endSecond <= rightChild.beginFirst &&
                                                          leftChild.firstSecond && rightChild.firstSecond) ||
                                                         (leftChild.endSecond <= rightChild.beginSecond &&
                                                          leftChild.firstSecond && rightChild.secondSecond));

        vectorRepresentation[p_nodeIndex].secondFirst = leftChild.beginSecond <= rightChild.endFirst &&
                                                        ((leftChild.endFirst <= rightChild.beginFirst &&
                                                          leftChild.secondFirst && rightChild.firstFirst) ||
                                                         (leftChild.endFirst <= rightChild.beginSecond &&
                                                          leftChild.secondFirst && rightChild.secondFirst) ||
                                                         (leftChild.endSecond <= rightChild.beginFirst &&
                                                          leftChild.secondSecond && rightChild.firstFirst) ||
                                                         (leftChild.endSecond <= rightChild.beginSecond &&
                                                          leftChild.secondSecond && rightChild.secondFirst));

        vectorRepresentation[p_nodeIndex].secondSecond = leftChild.beginSecond <= rightChild.endSecond &&
                                                         ((leftChild.endFirst <= rightChild.beginFirst &&
                                                           leftChild.secondFirst && rightChild.firstSecond) ||
                                                          (leftChild.endFirst <= rightChild.beginSecond &&
                                                           leftChild.secondFirst && rightChild.secondSecond) ||
                                                          (leftChild.endSecond <= rightChild.beginFirst &&
                                                           leftChild.secondSecond && rightChild.firstSecond) ||
                                                          (leftChild.endSecond <= rightChild.beginSecond &&
                                                           leftChild.secondSecond && rightChild.secondSecond));
    }

public:

    intervalTree(int p_size, int p_numberOfCards) : vectorRepresentation(p_size) {
        for(int i = p_size >> 1; i < (p_size >> 1) + p_numberOfCards; ++i) {
            cin >> vectorRepresentation[i].beginFirst >> vectorRepresentation[i].beginSecond;
            vectorRepresentation[i].endFirst = vectorRepresentation[i].beginFirst;
            vectorRepresentation[i].endSecond = vectorRepresentation[i].beginSecond;
        }

        for(int i = (p_size >> 1) - 1; i > 0; --i) {
            updateNode(i);
        }
    }

    void swap(int p_first, int p_second) {
        int first = p_first + (vectorRepresentation.size() >> 1) - 1;
        int second = p_second + (vectorRepresentation.size() >> 1) - 1;

        node temp = vectorRepresentation[first];
        vectorRepresentation[first] = vectorRepresentation[second];
        vectorRepresentation[second] = temp;

        first = parent(first);
        second = parent(second);

        while(first != second) {
            updateNode(first);
            updateNode(second);

            first = parent(first);
            second = parent(second);
        }

        while(first > 0) {
            updateNode(first);
            first = parent(first);
        }

        if(vectorRepresentation[1].firstFirst || vectorRepresentation[1].firstSecond ||
           vectorRepresentation[1].secondFirst || vectorRepresentation[1].secondSecond) {

            cout << "TAK" << endl;
        } else {
            cout << "NIE" << endl;
        }
    }
};


int findIntervalTreeSize(int p_numberOfCards) {
    int result = 1;
    while(result < p_numberOfCards) {
        result <<= 1;
    }
    return result << 1;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfCards;
        cin >> numberOfCards;

        intervalTree cardsTree(findIntervalTreeSize(numberOfCards), numberOfCards);

        int numberOfSwaps;
        cin >> numberOfSwaps;

        while(numberOfSwaps--) {
            int first, second;
            cin >> first >> second;
            cardsTree.swap(first, second);
        }
    }
}