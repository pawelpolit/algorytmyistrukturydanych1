/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <list>

using namespace std;

void loadRequests(vector<int>& p_requests, vector<list<int>>& p_furthestInFuture) {
    for(int i = 0; i < p_requests.size(); ++i) {
        cin >> p_requests[i];
        p_furthestInFuture[p_requests[i]].push_back(i);
    }
}

vector<int>::iterator fillCache(vector<bool>& p_inCache, vector<int>& p_cache, vector<int>& p_requests,
                                int& p_memoryAccessing, vector<list<int>>& p_furthestInFuture) {

    int toFill = p_cache.size();
    vector<int>::iterator nextRequest = p_requests.begin();

    while(toFill > 0 && nextRequest != p_requests.end()) {
        if(!p_inCache[*nextRequest]) {
            p_cache[--toFill] = *nextRequest;
            p_inCache[*nextRequest] = true;
            ++p_memoryAccessing;
        }
        p_furthestInFuture[*nextRequest].pop_front();
        ++nextRequest;
    }

    return nextRequest;
}

int findFurthestInFutureCellCacheIndex(vector<int>& p_cache, vector<list<int>>& p_furthestInFuture) {

    int max = 0, bestCellIndex = 0;

    for(int i = 0; i < p_cache.size(); ++i) {
        if(p_furthestInFuture[p_cache[i]].empty()) {
            return i;
        } else if(p_furthestInFuture[p_cache[i]].front() > max) {
            max = p_furthestInFuture[p_cache[i]].front();
            bestCellIndex = i;
        }
    }

    return bestCellIndex;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int cacheSize, memorySize, numberOfRequests;
        cin >> cacheSize >> memorySize >> numberOfRequests;

        vector<int> requests(numberOfRequests);
        vector<list<int>> furthestInFuture(memorySize + 1);
        loadRequests(requests, furthestInFuture);

        vector<int> cache(cacheSize);
        vector<bool> inCache(memorySize + 1, false);
        int memoryAccessing = 0;
        vector<int>::iterator nextRequest = fillCache(inCache, cache, requests, memoryAccessing, furthestInFuture);

        while(nextRequest != requests.end()) {
            if(!inCache[*nextRequest]) {
                int cellToRemoveIndex = findFurthestInFutureCellCacheIndex(cache, furthestInFuture);

                inCache[cache[cellToRemoveIndex]] = false;
                inCache[*nextRequest] = true;
                cache[cellToRemoveIndex] = *nextRequest;
                ++memoryAccessing;
            }
            furthestInFuture[*nextRequest].pop_front();
            ++nextRequest;
        }

        cout << memoryAccessing << endl;
    }
}