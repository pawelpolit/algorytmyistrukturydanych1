/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <queue>

using namespace std;


struct road {
    int first, second;
    long long cost;

    road(int p_first, int p_second, long long p_cost) : first(p_first), second(p_second), cost(p_cost) {}
};


struct road_comparator {
    bool operator()(road& p_first, road& p_second) {
        return p_first.cost > p_second.cost;
    }
};

typedef priority_queue<road, vector<road>, road_comparator> roads_queue;


void fillWithIndices(vector<int>& p_vector) {
    for(int i = 0; i < p_vector.size(); ++i) {
        p_vector[i] = i;
    }
}


void loadRoads(int p_numberOfRoads, roads_queue& p_roadsQueue) {
    while(p_numberOfRoads--) {
        int first, second, cost;
        cin >> first >> second >> cost;

        p_roadsQueue.push(road(first, second, cost));
    }
}


int findSet(int p_element, vector<int>& p_parent) {
    if(p_parent[p_element] != p_element) {
        p_parent[p_element] = findSet(p_parent[p_element], p_parent);
    }
    return p_parent[p_element];
}


void setsUnion(int p_first, int p_second, vector<int>& p_parent) {
    p_parent[p_first] = p_second;
}


long long findMinCost(roads_queue& p_roadsQueue, vector<int>& p_parent) {
    long long minCost = 0;

    while(!p_roadsQueue.empty()) {
        road currentRoad = p_roadsQueue.top();
        p_roadsQueue.pop();

        int firstSet = findSet(currentRoad.first, p_parent);
        int secondSet = findSet(currentRoad.second, p_parent);

        if(firstSet != secondSet) {
            setsUnion(firstSet, secondSet, p_parent);
            minCost += currentRoad.cost;
        }
    }

    return minCost;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfTramStops, numberOfRoads;
        cin >> numberOfTramStops >> numberOfRoads;

        vector<int> parent(numberOfTramStops + 1);
        fillWithIndices(parent);

        roads_queue roadsQueue;
        loadRoads(numberOfRoads, roadsQueue);

        cout << findMinCost(roadsQueue, parent) << endl;
    }
}