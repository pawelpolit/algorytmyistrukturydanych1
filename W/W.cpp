/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <limits>
#include <queue>

using namespace std;

struct indexAndDistance {
    int index, distance;

    indexAndDistance(int p_nextRoom, int p_distance) : index(p_nextRoom), distance(p_distance) {}
};

struct comparator {

    bool operator()(pair<int, int>& p_first, pair<int, int>& p_second) {
        return p_first.second > p_second.second;
    }
};

typedef priority_queue<pair<int, int>, vector<pair<int, int>>, comparator> my_queue;


void loadGraph(int p_numberOfCorridors, vector<vector<indexAndDistance>>& p_neighbours) {
    while(p_numberOfCorridors--) {
        int first, second, distance;
        cin >> first >> second >> distance;

        p_neighbours[first].push_back(indexAndDistance(second, distance));
    }
}


void fillQueue(my_queue& p_roomsQueue, vector<int>& p_distanceFromSource) {
    for(int i = 1; i < p_distanceFromSource.size(); ++i) {
        p_roomsQueue.push(pair<int, int>(i, p_distanceFromSource[i]));
    }
}


void relax(int p_first, int p_second, int p_distanceBetweenFirstAndSecond, vector<int>& p_distanceFromSource,
           my_queue& p_roomsQueue) {

    if(p_distanceFromSource[p_first] < numeric_limits<int>::max() &&
       p_distanceFromSource[p_second] > p_distanceFromSource[p_first] + p_distanceBetweenFirstAndSecond) {

        p_distanceFromSource[p_second] = p_distanceFromSource[p_first] + p_distanceBetweenFirstAndSecond;
        p_roomsQueue.push(pair<int, int>(p_second, p_distanceFromSource[p_second]));
    }
}


int findDistanceToExit(vector<vector<indexAndDistance>>& p_neighbours) {
    vector<int> distanceFromSource(p_neighbours.size(), numeric_limits<int>::max());
    distanceFromSource[1] = 0;

    vector<bool> hasBestDistance(p_neighbours.size(), false);

    my_queue roomsQueue;
    fillQueue(roomsQueue, distanceFromSource);

    while(!roomsQueue.empty()) {
        int currentRoom = roomsQueue.top().first;
        roomsQueue.pop();

        if(currentRoom == p_neighbours.size() - 1) {
            break;
        }

        if(hasBestDistance[currentRoom]) {
            continue;
        }

        hasBestDistance[currentRoom] = true;

        for(auto& neighbour : p_neighbours[currentRoom]) {
            if(!hasBestDistance[neighbour.index]) {
                relax(currentRoom, neighbour.index, neighbour.distance, distanceFromSource, roomsQueue);
            }
        }
    }

    return distanceFromSource.back();
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfRooms, numberOfCorridors;
        cin >> numberOfRooms >> numberOfCorridors;

        vector<vector<indexAndDistance>> neighbours(numberOfRooms + 1);
        loadGraph(numberOfCorridors, neighbours);

        int distanceToExit = findDistanceToExit(neighbours);

        if(distanceToExit == numeric_limits<int>::max()) {
            cout << "BRAK" << endl;
        } else {
            cout << distanceToExit << endl;
        }
    }
}