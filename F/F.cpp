#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void readHeights(vector<int>& p_height) {
	
	for (int i = 0; i < p_height.size(); i++) {
		cin >> p_height[i];
	}
}


int main() {
	ios_base::sync_with_stdio(false);

	int z;
	cin >> z;

	while (z--) {
		int number;
		cin >> number;
		
		vector<int> height(number);
		readHeights(height);

		vector<int> smallestLastNumberInGrowingSequence;
		smallestLastNumberInGrowingSequence.push_back(height[0]);

		for (int i = 1; i < number; i++) {
			vector<int>::iterator positionToChange = upper_bound(smallestLastNumberInGrowingSequence.begin(),
											   smallestLastNumberInGrowingSequence.end(),
											   height[i]);
			if (positionToChange < smallestLastNumberInGrowingSequence.end()) {
				*positionToChange = height[i];
			} else {
				smallestLastNumberInGrowingSequence.push_back(height[i]);
			}
		}

		cout << number - smallestLastNumberInGrowingSequence.size() << endl;
	}
}