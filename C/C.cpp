#include <iostream>
using namespace std;

void prepareItemsData(int* p_itemWeight, int* p_itemValue, int p_numberOfItems) {
	p_itemWeight[0] = 0;
	p_itemValue[0] = 0;
	for (int i = 1; i <= p_numberOfItems; i++) {
		cin >> p_itemWeight[i] >> p_itemValue[i];
	}
}

int** prepareMatrix(int p_height, int p_width) {
	int** matrix = new int*[p_height];
	for (int i = 0; i < p_height; i++) {
		matrix[i] = new int[p_width];
	}

	for (int i = 0; i < p_width; i++) {
		matrix[0][i] = 0;
	}
	return matrix;
}

void deleteMatrix(int** p_matrix, int p_height) {
	for (int i = 0; i < p_height; i++) {
		delete[] p_matrix[i];
	}
	delete[] p_matrix;
}

int max(int p_first, int p_second) {
	return (p_first > p_second) ? p_first : p_second;
}


int main() {
	ios_base::sync_with_stdio(false);

	int z;
	cin >> z;
	while (z--) {
		int numberOfItems, backpackCapacity;
		cin >> numberOfItems >> backpackCapacity;

		int* itemWeight = new int[numberOfItems + 1];
		int* itemValue = new int[numberOfItems + 1];
		prepareItemsData(itemWeight, itemValue, numberOfItems);

		int** profitItemsCapacity = prepareMatrix(2, backpackCapacity + 1);

		for (int currentItemsNumber = 1; currentItemsNumber <= numberOfItems; currentItemsNumber++) {
			int matrixRow = (currentItemsNumber - 1) % 2;

			for (int currentBackpackCapacity = 0; currentBackpackCapacity <= backpackCapacity; currentBackpackCapacity++) {

				int profitWithThisItem = 0;
				int remainingCapacityWithThisItem = currentBackpackCapacity - itemWeight[currentItemsNumber];

				if (remainingCapacityWithThisItem >= 0) {
					profitWithThisItem = itemValue[currentItemsNumber] + profitItemsCapacity[matrixRow][remainingCapacityWithThisItem];
				}

				profitItemsCapacity[currentItemsNumber % 2][currentBackpackCapacity] = max(profitItemsCapacity[matrixRow][currentBackpackCapacity],
																						   profitWithThisItem);
			}
		}

		cout << profitItemsCapacity[numberOfItems % 2][backpackCapacity] << endl;

		delete[] itemWeight;
		delete[] itemValue;
		deleteMatrix(profitItemsCapacity, 2);
	}
}