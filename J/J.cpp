/*
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>

using namespace std;

struct Data {
    vector<long long>& costOf;
    vector<vector<int>>& neighboursOfNode;
    vector<long long>& costOfSubtreeWithRoot;
    vector<long long>& costOfSubtreeWithoutRoot;

    Data(vector<long long>& p_costOf, vector<vector<int>>& p_neighboursOfNode,
         vector<long long>& p_costOfSubtreeWithRoot,
         vector<long long>& p_costOfSubtreeWithoutRoot) : costOf(p_costOf), neighboursOfNode(p_neighboursOfNode),
                                                          costOfSubtreeWithRoot(p_costOfSubtreeWithRoot),
                                                          costOfSubtreeWithoutRoot(p_costOfSubtreeWithoutRoot) {}
};


void loadCost(vector<long long>& p_costOf) {
    for(int i = 1; i < p_costOf.size(); ++i) {
        cin >> p_costOf[i];
    }
}

void loadTree(vector<vector<int>>& p_neighboursOfNode) {
    int numberOfEdges = p_neighboursOfNode.size() - 2;

    while(numberOfEdges--) {
        int first, second;
        cin >> first >> second;

        p_neighboursOfNode[first].push_back(second);
        p_neighboursOfNode[second].push_back(first);
    }
}

long long min(long long p_first, long long p_second) {
    return (p_first < p_second) ? p_first : p_second;
}

void computeCostOfSubtreeDFS(int p_root, int p_previous, Data& p_data) {

    long long valueWithoutRoot = 0;
    long long valueWithRoot = p_data.costOf[p_root];

    for(int neighbour : p_data.neighboursOfNode[p_root]) {

        if(neighbour != p_previous) {
            computeCostOfSubtreeDFS(neighbour, p_root, p_data);

            valueWithoutRoot += p_data.costOfSubtreeWithRoot[neighbour];
            valueWithRoot += min(p_data.costOfSubtreeWithRoot[neighbour],
                                 p_data.costOfSubtreeWithoutRoot[neighbour]);
        }
    }

    p_data.costOfSubtreeWithoutRoot[p_root] = valueWithoutRoot;
    p_data.costOfSubtreeWithRoot[p_root] = valueWithRoot;
}

void previousRootTakenDFS(int p_root, int p_previous, Data& p_data, vector<bool>& p_isTaken);

void previousRootNotTakenDFS(int p_root, int p_previous, Data& p_data, vector<bool>& p_isTaken) {
    p_isTaken[p_root] = true;

    for(int neighbour : p_data.neighboursOfNode[p_root]) {

        if(neighbour != p_previous) {
            previousRootTakenDFS(neighbour, p_root, p_data, p_isTaken);
        }
    }
}

void previousRootTakenDFS(int p_root, int p_previous, Data& p_data, vector<bool>& p_isTaken) {

    void (* nextFunction)(int, int, Data&, vector<bool>&);

    if(p_data.costOfSubtreeWithRoot[p_root] < p_data.costOfSubtreeWithoutRoot[p_root]) {
        p_isTaken[p_root] = true;
        nextFunction = previousRootTakenDFS;
    } else {
        p_isTaken[p_root] = false;
        nextFunction = previousRootNotTakenDFS;
    }

    for(int neighbour : p_data.neighboursOfNode[p_root]) {

        if(neighbour != p_previous) {
            nextFunction(neighbour, p_root, p_data, p_isTaken);
        }
    }
}

void printResult(vector<bool>& p_isTaken) {
    for(int i = 1; i < p_isTaken.size(); ++i) {
        cout << p_isTaken[i];
    }
    cout << endl;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfNodes;
        cin >> numberOfNodes;

        vector<long long> costOf(numberOfNodes + 1);
        loadCost(costOf);

        vector<vector<int>> neighboursOfNode(numberOfNodes + 1);
        loadTree(neighboursOfNode);

        vector<long long> costOfSubtreeWithRoot(numberOfNodes + 1);
        vector<long long> costOfSubtreeWithoutRoot(numberOfNodes + 1);

        Data data(costOf, neighboursOfNode, costOfSubtreeWithRoot, costOfSubtreeWithoutRoot);

        computeCostOfSubtreeDFS(1, 0, data);
        cout << min(costOfSubtreeWithRoot[1], costOfSubtreeWithoutRoot[1]) << endl;

        vector<bool> isTaken(numberOfNodes + 1);

        previousRootTakenDFS(1, 0, data, isTaken);

        printResult(isTaken);
    }
}