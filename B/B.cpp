#include <iostream>
using namespace std;


void prepareHolesData(long p_numberOfHoles, long* p_holePosition, long* p_holeCost) {
	p_holePosition[0] = 0;
	p_holeCost[0] = 0;
	for (long i = 1; i <= p_numberOfHoles; i++) {
		cin >> p_holePosition[i] >> p_holeCost[i];
	}
}

long min(long p_first, long p_second) {
	return (p_first < p_second) ? p_first : p_second;
}

long findIndexOfLastPositionBeforePatch(long* p_holePosition, long p_beginningOfPatchPosition, long p_currentIterator) {
	while (p_holePosition[p_currentIterator + 1] < p_beginningOfPatchPosition) {
		p_currentIterator++;
	}
	return p_currentIterator;
}


int main() {
	ios_base::sync_with_stdio(false);

	long z;
	cin >> z;
	while (z--) {
		long numberOfHoles;
		cin >> numberOfHoles;

		long* holePosition = new long[numberOfHoles + 1];
		long* holeCost = new long[numberOfHoles + 1];
		prepareHolesData(numberOfHoles, holePosition, holeCost);

		long patchLength, patchCost;
		cin >> patchLength >> patchCost;

		long* totalCost = new long[numberOfHoles + 1];
		totalCost[0] = 0;

		for (long i = 1, positionOfIteratingOverHoles = 0; i <= numberOfHoles; i++) {
			positionOfIteratingOverHoles = findIndexOfLastPositionBeforePatch(holePosition, 
																			  holePosition[i] - patchLength, 
																			  positionOfIteratingOverHoles);
			totalCost[i] = min(totalCost[i - 1] + holeCost[i],
							   patchCost + totalCost[positionOfIteratingOverHoles]);
		}

		cout << totalCost[numberOfHoles] << endl;

		delete[] holePosition;
		delete[] holeCost;
		delete[] totalCost;
	}
}