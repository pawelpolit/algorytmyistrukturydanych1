/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void loadWarriors(vector<int>& p_warriors) {
    for(int i = 0; i < p_warriors.size(); ++i) {
        cin >> p_warriors[i];
    }
}

int computeProfit(vector<int>& p_orcs, vector<int>& p_elves) {
    vector<int>::iterator beginOrcs = p_orcs.begin();
    vector<int>::iterator endOrcs = --p_orcs.end();
    vector<int>::iterator beginElves = p_elves.begin();
    vector<int>::iterator endElves = --p_elves.end();

    int profit = 0;

    while(beginOrcs <= endOrcs) {
        if(*endOrcs < *beginElves) {
            profit -= (endOrcs - beginOrcs + 1) * 200;
            break;
        } else if(*endElves < *beginOrcs) {
            profit += (endOrcs - beginOrcs + 1) * 200;
            break;
        } else if(*endOrcs > *endElves) {
            --endOrcs;
            --endElves;
            profit += 200;
        } else if(*endOrcs < *endElves) {
            ++beginOrcs;
            --endElves;
            profit -= 200;
        } else if(*beginOrcs > *beginElves) {
            ++beginOrcs;
            ++beginElves;
            profit += 200;
        } else if(*beginOrcs < *beginElves) {
            ++beginOrcs;
            --endElves;
            profit -= 200;
        } else {
            if(*beginOrcs < *endElves) {
                profit -= 200;
            }
            ++beginOrcs;
            --endElves;
        }
    }

    return profit;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfWarriors;
        cin >> numberOfWarriors;

        vector<int> orcs(numberOfWarriors);
        loadWarriors(orcs);
        sort(orcs.begin(), orcs.end());

        vector<int> elves(numberOfWarriors);
        loadWarriors(elves);
        sort(elves.begin(), elves.end());

        cout << computeProfit(orcs, elves) << endl;
    }
}