/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>

using namespace std;

vector<int>* graph;
vector<int>* transposed;
int* order;
int* sss;
bool* visited;
int number;


void dfs(int x) {
    if(visited[x]) {
        return;
    }
    visited[x] = true;

    for(int i = 0; i < graph[x].size(); i++) {
        dfs(graph[x][i]);
    }

    order[number++] = x;
}


void dfs2(int x) {
    if(visited[x]) {
        return;
    }
    visited[x] = true;

    for(int i = 0; i < transposed[x].size(); i++) {
        dfs2(transposed[x][i]);
    }

    sss[x] = number;
}


int main() {
    ios_base::sync_with_stdio(false);
    int z;

    cin >> z;
    while(z--) {
        int n, m, k, value = 0;
        cin >> n >> m;
        ++n;

        graph = new vector<int>[n];
        transposed = new vector<int>[n];
        order = new int[n];
        sss = new int[n];
        visited = new bool[n];

        for(int i = 0; i < n; i++) {
            visited[i] = false;
        }

        while(m--) {
            int first, second;
            cin >> first >> second;

            graph[first].push_back(second);
            transposed[second].push_back(first);
        }

        number = 0;

        for(int i = 1; i < n; i++) {
            if(!visited[i]) {
                dfs(i);
            }
        }

        number = 0;
        for(int i = 0; i < n; i++) {
            visited[i] = false;
        }

        for(int i = n - 2; i >= 0; i--) {
            if(!visited[order[i]]) {
                dfs2(order[i]);
                number++;
            }
        }

        delete[] graph;
        delete[] transposed;
        delete[] order;
        delete[] visited;

        cin >> k;
        while(k--) {
            int first, second;
            cin >> first >> second;

            if(first == second || sss[first] == sss[second]) {
                cout << "TAK" << endl;
            } else {
                cout << "NIE" << endl;
            }
        }

        delete[] sss;
    }
}