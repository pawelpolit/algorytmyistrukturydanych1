/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <unordered_map>
#include <vector>
#include <limits>

using namespace std;


void loadCities(int p_numberOfCities, vector<string>& p_cities, unordered_map<string, int>& p_cityToIndex) {
    for(int i = 0; i < p_numberOfCities; ++i) {
        cin >> p_cities[i];
        p_cityToIndex[p_cities[i]] = i;
    }
}


void loadRoads(int p_numberOfHighways, vector<vector<int>>& p_bestDistanceBetween,
               unordered_map<string, int>& p_cityToIndex) {

    for(int i = 0; i < p_bestDistanceBetween.size(); ++i) {
        p_bestDistanceBetween[i][i] = 0;
    }

    while(p_numberOfHighways--) {
        string firstCity, secondCity;
        int distance;
        cin >> firstCity >> secondCity >> distance;

        int firstCityIndex = p_cityToIndex[firstCity];
        int secondCityIndex = p_cityToIndex[secondCity];

        p_bestDistanceBetween[firstCityIndex][secondCityIndex] = distance;
        p_bestDistanceBetween[secondCityIndex][firstCityIndex] = distance;
    }
}


int min(int p_first, int p_second) {
    return (p_first < p_second) ? p_first : p_second;
}


int addDistances(int p_first, int p_second) {
    if(p_first == numeric_limits<int>::max() || p_second == numeric_limits<int>::max()) {
        return numeric_limits<int>::max();
    } else {
        return p_first + p_second;
    }
}


void warshallFloyd(vector<vector<int>>& p_bestDistanceBetween) {
    for(int k = 0; k < p_bestDistanceBetween.size(); ++k) {
        for(int i = 0; i < p_bestDistanceBetween.size(); ++i) {
            for(int j = 0; j < i; ++j) {
                p_bestDistanceBetween[i][j] = min(p_bestDistanceBetween[i][j],
                                                  addDistances(p_bestDistanceBetween[i][k],
                                                               p_bestDistanceBetween[k][j]));
                p_bestDistanceBetween[j][i] = p_bestDistanceBetween[i][j];
            }
        }
    }
}


void printResult(vector<string>& p_cities, unordered_map<string, int>& p_cityToIndex,
                 vector<vector<int>>& p_bestDistanceBetween) {

    for(string& city : p_cities) {
        cout << city << ' ';
    }
    cout << endl;

    for(string& city : p_cities) {
        cout << city << ' ';

        for(int& distance : p_bestDistanceBetween[p_cityToIndex[city]]) {
            cout << distance << ' ';
        }

        cout << endl;
    }
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfCities;
        cin >> numberOfCities;

        vector<string> cities(numberOfCities);
        unordered_map<string, int> cityToIndex;
        loadCities(numberOfCities, cities, cityToIndex);

        int numberOfHighways;
        cin >> numberOfHighways;

        vector<vector<int>> bestDistanceBetween(numberOfCities,
                                                vector<int>(numberOfCities, numeric_limits<int>::max()));
        loadRoads(numberOfHighways, bestDistanceBetween, cityToIndex);

        warshallFloyd(bestDistanceBetween);

        printResult(cities, cityToIndex, bestDistanceBetween);
    }
}