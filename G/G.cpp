/*
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>

using namespace std;

int preparePizzaAndGetNumberOfOlives(vector<int>& p_numberOfOlivesOnPiece, vector<int>& p_numberOfOlivesTo) {
    int numberOfOlives = 0;
    for(int i = 0; i < p_numberOfOlivesOnPiece.size(); ++i) {
        cin >> p_numberOfOlivesOnPiece[i];
        numberOfOlives += p_numberOfOlivesOnPiece[i];
        p_numberOfOlivesTo[i] = numberOfOlives;
    }
    return numberOfOlives;
}

void fillTable(vector<vector<int>>& p_table) {
    for(int i = 0; i < p_table.size(); ++i) {
        for(int j = 0; j < p_table.size(); ++j) {
            p_table[i].push_back(0);
        }
    }
}

int numberOfOlives(vector<int>& p_numberOfOlivesTo, int p_begin, int p_end) {
    if(p_begin == 0) {
        return p_numberOfOlivesTo[p_end];
    }

    if(p_begin < p_end) {
        return p_numberOfOlivesTo[p_end] - p_numberOfOlivesTo[p_begin - 1];
    } else {
        return p_numberOfOlivesTo.back() - p_numberOfOlivesTo[p_begin - 1] + p_numberOfOlivesTo[p_end];
    }
}

int min(int p_first, int p_second) {
    return (p_first < p_second) ? p_first : p_second;
}

void computeBestNumberOfOlives(vector<vector<int>>& p_bestNumberOfOlivesInRange, vector<int>& p_numberOfOlivesTo,
                               int p_numberOfPieces) {

    for(int distance = 1; distance < p_numberOfPieces - 1; ++distance) {
        for(int begin = 0; begin < p_numberOfPieces; ++begin) {
            int end = (begin + distance) % p_numberOfPieces;
            int ifFirst = p_bestNumberOfOlivesInRange[(begin + 1) % p_numberOfPieces][end];
            int ifLast = p_bestNumberOfOlivesInRange[begin][(p_numberOfPieces + end - 1) % p_numberOfPieces];

            p_bestNumberOfOlivesInRange[begin][end] =
                    numberOfOlives(p_numberOfOlivesTo, begin, end) - min(ifFirst, ifLast);

        }
    }
}

int findMinimumOfOlivesWithoutOnePiece(vector<vector<int>>& p_bestNumberOfOlivesInRange, int p_distance,
                                       int p_numberOfPieces) {

    int result = p_bestNumberOfOlivesInRange[0][p_distance];

    for(int begin = 1; begin < p_numberOfPieces; ++begin) {
        int end = (begin + p_distance) % p_numberOfPieces;

        if(p_bestNumberOfOlivesInRange[begin][end] < result) {
            result = p_bestNumberOfOlivesInRange[begin][end];
        }
    }
    return result;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfPieces;
        cin >> numberOfPieces;

        vector<int> numberOfOlivesOnPiece(numberOfPieces);
        vector<int> numberOfOlivesTo(numberOfPieces);
        int numberOfOlives = preparePizzaAndGetNumberOfOlives(numberOfOlivesOnPiece, numberOfOlivesTo);

        vector<vector<int>> bestNumberOfOlivesInRange(numberOfPieces);
        fillTable(bestNumberOfOlivesInRange);

        for(int i = 0; i < numberOfPieces; ++i) {
            bestNumberOfOlivesInRange[i][i] = numberOfOlivesOnPiece[i];
        }

        computeBestNumberOfOlives(bestNumberOfOlivesInRange, numberOfOlivesTo, numberOfPieces);

        int bajtazarOlives = findMinimumOfOlivesWithoutOnePiece(bestNumberOfOlivesInRange, numberOfPieces - 2,
                                                                numberOfPieces);

        cout << numberOfOlives - bajtazarOlives << ' ' << bajtazarOlives << endl;
    }
}