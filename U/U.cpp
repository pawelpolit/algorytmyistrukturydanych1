/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>

using namespace std;

long long dfsTime;

struct person {
    long long preorderTime;
    long long low;
    long long lostConnectionsWithoutThis = 0;
    bool visited = false;
};


void loadConnections(vector<vector<long long>>& p_connection, long long p_numberOfConnections) {
    long long first, second;

    while(p_numberOfConnections--) {
        cin >> first >> second;

        p_connection[first].push_back(second);
        p_connection[second].push_back(first);
    }
}

long long min(long long p_first, long long p_second) {
    return (p_first < p_second) ? p_first : p_second;
}

long long bic(long long p_current, long long p_parent, vector<person>& p_people,
              vector<vector<long long>>& p_connections) {

    p_people[p_current].visited = true;
    p_people[p_current].preorderTime = dfsTime;
    p_people[p_current].low = dfsTime++;

    long long numberOfPeopleInSubtree = 0;
    long long numberOfPeopleInSubtreeWithoutBackEdges = 0;

    bool moreThenOneBranch = false;
    for(long long child : p_connections[p_current]) {
        if(!p_people[child].visited) {
            long long numberOfPeopleInBranch = bic(child, p_current, p_people, p_connections);
            numberOfPeopleInSubtree += numberOfPeopleInBranch;

            if((p_current != 1 && p_people[child].low >= p_people[p_current].preorderTime) ||
               (p_current == 1 && moreThenOneBranch)) {

                numberOfPeopleInSubtreeWithoutBackEdges += numberOfPeopleInBranch;
                p_people[p_current].lostConnectionsWithoutThis +=
                        2 * numberOfPeopleInBranch * (p_people.size() - 2 - numberOfPeopleInSubtreeWithoutBackEdges);
            }

            p_people[p_current].low = min(p_people[p_current].low, p_people[child].low);
        } else if(p_people[p_current].preorderTime > p_people[child].preorderTime && child != p_parent) {
            p_people[p_current].low = min(p_people[p_current].low, p_people[child].preorderTime);
        }
        moreThenOneBranch = true;
    }

    return numberOfPeopleInSubtree + 1;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        long long numberOfPeople, numberOfConnections;
        cin >> numberOfPeople >> numberOfConnections;

        vector<vector<long long>> connections(numberOfPeople + 1);
        loadConnections(connections, numberOfConnections);
        vector<person> people(numberOfPeople + 1);

        dfsTime = 0;
        bic(1, 0, people, connections);

        long long allConnections = numberOfPeople * (numberOfPeople - 1);

        for(long long i = 1; i < people.size(); ++i) {
            cout << allConnections - 2 * (numberOfPeople - 1) - people[i].lostConnectionsWithoutThis << endl;
        }
    }
}