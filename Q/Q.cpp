/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>

using namespace std;


struct node {
    int begin, end;
    int add = 0, max = 0;
};


int left(int p_nodeIndex) {
    return 2 * p_nodeIndex;
}

int right(int p_nodeIndex) {
    return 2 * p_nodeIndex + 1;
}

int max(int p_first, int p_second) {
    return (p_first > p_second) ? p_first : p_second;
}

int findIntervalTreeSize(int p_numberOfCities) {
    int result = 1;
    while(result < p_numberOfCities) {
        result <<= 1;
    }
    return 2 * result;
}

void prepareIntervalTree(vector<node>& p_intervalTree) {
    for(int i = p_intervalTree.size() / 2, nextCity = 1; i < p_intervalTree.size(); ++i, ++nextCity) {
        p_intervalTree[i].begin = nextCity;
        p_intervalTree[i].end = nextCity;
    }
    for(int i = p_intervalTree.size() / 2 - 1; i > 0; --i) {
        p_intervalTree[i].begin = p_intervalTree[left(i)].begin;
        p_intervalTree[i].end = p_intervalTree[right(i)].end;
    }
}

bool canAddToInterval(int p_currentNode, int p_addFromPrevious, int p_begin, int p_end, int p_seats,
                      vector<node>& p_intervalTree, int p_maxNumberOfSeats) {

    if(p_intervalTree[p_currentNode].begin == p_begin && p_intervalTree[p_currentNode].end == p_end) {
        return p_intervalTree[p_currentNode].max + p_addFromPrevious + p_seats <= p_maxNumberOfSeats;
    }

    int nextAdd = p_addFromPrevious + p_intervalTree[p_currentNode].add;

    if(p_end <= p_intervalTree[left(p_currentNode)].end) {
        return canAddToInterval(left(p_currentNode), nextAdd, p_begin, p_end, p_seats, p_intervalTree,
                                p_maxNumberOfSeats);
    }
    if(p_begin >= p_intervalTree[right(p_currentNode)].begin) {
        return canAddToInterval(right(p_currentNode), nextAdd, p_begin, p_end, p_seats, p_intervalTree,
                                p_maxNumberOfSeats);
    }

    return canAddToInterval(left(p_currentNode), nextAdd, p_begin, p_intervalTree[left(p_currentNode)].end, p_seats,
                            p_intervalTree, p_maxNumberOfSeats) &&
           canAddToInterval(right(p_currentNode), nextAdd, p_intervalTree[right(p_currentNode)].begin, p_end, p_seats,
                            p_intervalTree, p_maxNumberOfSeats);
}

void addToInterval(int p_currentNode, int p_begin, int p_end, int p_seats, vector<node>& p_intervalTree) {
    if(p_intervalTree[p_currentNode].begin == p_begin && p_intervalTree[p_currentNode].end == p_end) {
        p_intervalTree[p_currentNode].add += p_seats;
        p_intervalTree[p_currentNode].max += p_seats;
        return;
    }

    if(p_end <= p_intervalTree[left(p_currentNode)].end) {
        addToInterval(left(p_currentNode), p_begin, p_end, p_seats, p_intervalTree);
    } else if(p_begin >= p_intervalTree[right(p_currentNode)].begin) {
        addToInterval(right(p_currentNode), p_begin, p_end, p_seats, p_intervalTree);
    } else {
        addToInterval(left(p_currentNode), p_begin, p_intervalTree[left(p_currentNode)].end, p_seats, p_intervalTree);
        addToInterval(right(p_currentNode), p_intervalTree[right(p_currentNode)].begin, p_end, p_seats, p_intervalTree);
    }

    p_intervalTree[p_currentNode].max = max(p_intervalTree[left(p_currentNode)].max,
                                            p_intervalTree[right(p_currentNode)].max) +
                                        p_intervalTree[p_currentNode].add;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfCities, maxNumberOfSeats, numberOfRequests;
        cin >> numberOfCities >> maxNumberOfSeats >> numberOfRequests;
        int numberOfRoads = numberOfCities - 1;

        vector<node> intervalTree(findIntervalTreeSize(numberOfRoads));
        prepareIntervalTree(intervalTree);

        while(numberOfRequests--) {
            int begin, end, seats;
            cin >> begin >> end >> seats;

            if(canAddToInterval(1, 0, begin, end - 1, seats, intervalTree, maxNumberOfSeats)) {

                addToInterval(1, begin, end - 1, seats, intervalTree);
                cout << "T" << endl;
            } else {
                cout << "N" << endl;
            }
        }
    }
}