/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

struct shop_data {
    int open, close, money;
};

class priorityQueueComparator {
    vector<shop_data>& shops;

public:

    priorityQueueComparator(vector<shop_data>& p_shops) : shops(p_shops) {}

    bool operator()(int p_first, int p_second) {
        return shops[p_first].close > shops[p_second].close;
    }
};

void loadShops(vector<shop_data>& p_shops) {
    for(shop_data& shop : p_shops) {
        cin >> shop.open >> shop.close >> shop.money;
    }
}

bool bestChoiceIsCorrect(vector<int> p_bestChoiceOfShops, vector<shop_data>& p_shops) {

    sort(p_bestChoiceOfShops.begin(), p_bestChoiceOfShops.end(), [&p_shops](int p_first, int p_second) {
        return p_shops[p_first].open < p_shops[p_second].open;
    });

    priorityQueueComparator comparator(p_shops);
    priority_queue<int, vector<int>, priorityQueueComparator> nextShop(comparator);
    vector<int>::iterator choiceOfShopsIterator = p_bestChoiceOfShops.begin();
    int time = p_shops[*choiceOfShopsIterator].open;

    while(choiceOfShopsIterator != p_bestChoiceOfShops.end()) {

        if(nextShop.empty() && time < p_shops[*choiceOfShopsIterator].open) {
            time = p_shops[*choiceOfShopsIterator].open;
        }

        while(choiceOfShopsIterator != p_bestChoiceOfShops.end() && p_shops[*choiceOfShopsIterator].open == time) {
            nextShop.push(*choiceOfShopsIterator);
            ++choiceOfShopsIterator;
        }

        if(p_shops[nextShop.top()].close > time) {
            nextShop.pop();
            ++time;
        } else {
            return false;
        }
    }

    while(!nextShop.empty()) {
        if(p_shops[nextShop.top()].close > time) {
            nextShop.pop();
            ++time;
        } else {
            return false;
        }
    }

    return true;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfShops;
        cin >> numberOfShops;

        vector<shop_data> shops(numberOfShops);
        loadShops(shops);
        sort(shops.begin(), shops.end(), [](shop_data p_first, shop_data p_second) {
            return p_first.money > p_second.money;
        });

        vector<int> bestChoiceOfShops;
        int maxMoney = 0;

        for(int shopIndex = 0; shopIndex < numberOfShops; ++shopIndex) {
            bestChoiceOfShops.push_back(shopIndex);
            if(!bestChoiceIsCorrect(bestChoiceOfShops, shops)) {
                bestChoiceOfShops.pop_back();
            } else {
                maxMoney += shops[shopIndex].money;
            }
        }

        cout << maxMoney << endl;
    }
}