/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <limits>
#include <queue>

using namespace std;


struct road {
    int target, distance, period, beginOfSafeTime, lengthOfSafeTime;


    road(int p_index, int p_distance, int p_period, int p_beginOfSafeTime, int p_lengthOfSafeTime) :
            target(p_index),
            distance(p_distance),
            period(p_period),
            beginOfSafeTime(p_beginOfSafeTime),
            lengthOfSafeTime(p_lengthOfSafeTime) {}

    int waitToSafeTime(int p_currentTime) {
        if(period == 0 && beginOfSafeTime == 0 && lengthOfSafeTime == 0) {
            return 0;
        }

        p_currentTime %= period;
        int endOfSafeTime = (beginOfSafeTime + lengthOfSafeTime) % period;

        if(beginOfSafeTime <= endOfSafeTime) {
            if(beginOfSafeTime <= p_currentTime && p_currentTime < endOfSafeTime) {
                return 0;
            } else {
                return (beginOfSafeTime + period - p_currentTime) % period;
            }
        } else {
            if(beginOfSafeTime <= p_currentTime || p_currentTime < endOfSafeTime) {
                return 0;
            } else {
                return beginOfSafeTime - p_currentTime;
            }
        }
    }
};

struct comparator {

    bool operator()(pair<int, int>& p_first, pair<int, int>& p_second) {
        return p_first.second > p_second.second;
    }
};

typedef priority_queue<pair<int, int>, vector<pair<int, int>>, comparator> my_queue;


void loadGraph(int p_numberOfRoads, vector<vector<road>>& p_roads) {
    while(p_numberOfRoads--) {
        int first, second, distance, period, beginOfSafeTime, lengthOfSafeTime;
        cin >> first >> second >> distance >> period >> beginOfSafeTime >> lengthOfSafeTime;

        p_roads[first].push_back(road(second, distance, period, beginOfSafeTime, lengthOfSafeTime));
    }
}


void fillQueue(my_queue& p_basesQueue, vector<int>& p_distanceFromBagdad) {
    for(int i = 1; i < p_distanceFromBagdad.size(); ++i) {
        p_basesQueue.push(pair<int, int>(i, p_distanceFromBagdad[i]));
    }
}


void relax(int p_first, int p_second, int p_distanceBetweenFirstAndSecond, vector<int>& p_distanceFromBagdad,
           my_queue& p_basesQueue) {

    if(p_distanceFromBagdad[p_first] < numeric_limits<int>::max() &&
       p_distanceFromBagdad[p_second] > p_distanceFromBagdad[p_first] + p_distanceBetweenFirstAndSecond) {

        p_distanceFromBagdad[p_second] = p_distanceFromBagdad[p_first] + p_distanceBetweenFirstAndSecond;
        p_basesQueue.push(pair<int, int>(p_second, p_distanceFromBagdad[p_second]));
    }
}


int findDistanceToBabilon(vector<vector<road>>& p_roads) {
    vector<int> bestDistanceFromBagdad(p_roads.size(), numeric_limits<int>::max());
    bestDistanceFromBagdad[1] = 0;

    vector<bool> hasBestDistance(p_roads.size(), false);

    my_queue basesQueue;
    fillQueue(basesQueue, bestDistanceFromBagdad);

    while(!basesQueue.empty()) {
        int currentBase = basesQueue.top().first;
        basesQueue.pop();

        if(currentBase == p_roads.size() - 1) {
            break;
        }

        if(hasBestDistance[currentBase]) {
            continue;
        }

        hasBestDistance[currentBase] = true;

        for(auto& road : p_roads[currentBase]) {
            if(!hasBestDistance[road.target]) {
                int distance = road.waitToSafeTime(bestDistanceFromBagdad[currentBase]) + road.distance;
                relax(currentBase, road.target, distance, bestDistanceFromBagdad, basesQueue);
            }
        }
    }

    return bestDistanceFromBagdad.back();
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfBases, numberOfRoads;
        cin >> numberOfBases >> numberOfRoads;

        vector<vector<road>> roads(numberOfBases + 1);
        loadGraph(numberOfRoads, roads);

        int distanceToBabilon = findDistanceToBabilon(roads);

        if(distanceToBabilon == numeric_limits<int>::max()) {
            cout << "NIE" << endl;
        } else {
            cout << distanceToBabilon << endl;
        }
    }
}