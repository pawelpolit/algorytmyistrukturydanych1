/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>

using namespace std;

void loadBoard(vector<vector<bool>>& p_holeOnField) {
    for(int i = 0; i < p_holeOnField.size(); ++i) {
        for(int j = 0; j < p_holeOnField.size(); ++j) {
            char field;
            cin >> field;
            p_holeOnField[i].push_back(field == 'o');
        }
    }
}

int cardinalityOf(int p_set) {

    int result = 0;

    while(p_set) {
        result += p_set & 1;
        p_set >>= 1;
    }

    return result;
}

void computeForSubset(int p_subset, vector<vector<bool>>& p_holeOnField,
                      vector<long long>& p_numberOfTowerSettingsInSubset) {

    int cardinalityOfSubset = cardinalityOf(p_subset);
    long long result = 0;

    for(int element = 1, shift = 0; element <= p_subset; element <<= 1, ++shift) {
        if(element & p_subset && !p_holeOnField[cardinalityOfSubset - 1][shift]) {
            result += p_numberOfTowerSettingsInSubset[p_subset - element];
        }
    }

    p_numberOfTowerSettingsInSubset[p_subset] = result;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {

        int boardSize;
        cin >> boardSize;

        vector<vector<bool>> holeOnField(boardSize);
        loadBoard(holeOnField);

        int numberOfSubsets = 1 << boardSize;
        vector<long long> numberOfTowerSettingsInSubset(numberOfSubsets);
        numberOfTowerSettingsInSubset[0] = 1;

        for(int subset = 1; subset < numberOfSubsets; ++subset) {
            computeForSubset(subset, holeOnField, numberOfTowerSettingsInSubset);
        }

        cout << numberOfTowerSettingsInSubset[numberOfSubsets - 1] << endl;
    }
}