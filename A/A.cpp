#include <iostream>
using namespace std;


void handleTrivialData(int p_numberOfPillars) {
	if (p_numberOfPillars == 1) {
		int result;
		cin >> result;
		cout << result << endl;
	} else {
		cout << 0 << endl;
	}
}

int* preparePillarsValues(int p_numberOfPillars) {
	int* pillarsValues = new int[p_numberOfPillars + 1];
	pillarsValues[0] = 0;

	for (int i = 1; i <= p_numberOfPillars; i++) {
		cin >> pillarsValues[i];
	}

	return pillarsValues;
}

int max(int p_first, int p_second, int p_third) {
	int max = (p_first > p_second) ? p_first : p_second;
	return (max > p_third) ? max : p_third;
}


int main() {
	ios_base::sync_with_stdio(false);

	int z;
	cin >> z;
	while (z--) {
		int numberOfPillars;
		cin >> numberOfPillars;

		if (numberOfPillars < 2) {
			handleTrivialData(numberOfPillars);
			continue;
		}

		int* pillarsValues = preparePillarsValues(numberOfPillars);
		int* greatestProfit = new int[numberOfPillars + 1];
		greatestProfit[0] = 0;
		greatestProfit[1] = pillarsValues[1];
		greatestProfit[2] = greatestProfit[1] + pillarsValues[2];

		for (int i = 3; i <= numberOfPillars; i++) {
			greatestProfit[i] = max(greatestProfit[i - 1],
									greatestProfit[i - 2] + pillarsValues[i],
									greatestProfit[i - 3] + pillarsValues[i - 1] + pillarsValues[i]);
		}

		cout << greatestProfit[numberOfPillars] << endl;

		delete[] pillarsValues;
		delete[] greatestProfit;
	}
}