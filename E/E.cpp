#include <iostream>
using namespace std;

pair<long long, long long>* prepareNodes(int p_numberOfNodes) {

	pair<long long, long long>* result = new pair<long long, long long>[p_numberOfNodes];

	for (int i = 0; i < p_numberOfNodes; i++) {
		cin >> result[i].first >> result[i].second;
	}

	return result;
}

long long** prepareSquareMatrix(int p_dimension) {
	long long** result = new long long*[p_dimension];

	for (int i = 0; i < p_dimension; i++) {
		result[i] = new long long[p_dimension];
	}

	for (int i = 1; i < p_dimension; i++) {
		result[i - 1][i] = 0;
	}

	return result;
}

void deleteMatrix(long long** p_matrix, int p_dimension) {
	for (int i = 0; i < p_dimension; i++) {
		delete[] p_matrix[i];
	}

	delete[] p_matrix;
}

long long square(long long p_number) {
	return p_number * p_number;
}

long long euclideanDistanceSquare(pair<long long, long long>& p_first, pair<long long, long long>& p_second) {
	return square(p_first.first - p_second.first) + square(p_first.second - p_second.second);
}

void printBestDiagonal(long long** p_bestResult, int p_begin, int p_end) {

	if (p_end - p_begin < 3) {
		return;
	}

	int temporaryBest = p_begin + 1;
	long long temporaryBestValue = p_bestResult[p_begin][temporaryBest] + p_bestResult[temporaryBest][p_end];

	for (int candidate = p_begin + 2; candidate < p_end; candidate++) {

		long long toCheck = p_bestResult[p_begin][candidate] + p_bestResult[candidate][p_end];

		if (toCheck < temporaryBestValue) {
			temporaryBestValue = toCheck;
			temporaryBest = candidate;
		}
	}

	if (temporaryBest - p_begin > 1) {
		cout << ' ' << p_begin << ' ' << temporaryBest;
	}

	if (p_end - temporaryBest > 1) {
		cout << ' ' << temporaryBest << ' ' << p_end;
	}

	printBestDiagonal(p_bestResult, p_begin, temporaryBest);
	printBestDiagonal(p_bestResult, temporaryBest, p_end);
}


int main() {
	ios_base::sync_with_stdio(false);

	int z;
	cin >> z;

	while (z--) {
		
		int numberOfNodes;
		cin >> numberOfNodes;

		pair<long long, long long>* nodes = prepareNodes(numberOfNodes);

		long long** bestResult = prepareSquareMatrix(numberOfNodes);

		for (int distance = 2; distance < numberOfNodes; distance++) {
			for (int nodeIndex = 0; nodeIndex + distance < numberOfNodes; nodeIndex++) {

				long long temporaryBest = bestResult[nodeIndex][nodeIndex + 1] + bestResult[nodeIndex + 1][nodeIndex + distance];

				for (int candidate = nodeIndex + 2; candidate < nodeIndex + distance; candidate++) {

					long long toCheck = bestResult[nodeIndex][candidate] + bestResult[candidate][nodeIndex + distance];

					if (toCheck < temporaryBest) {
						temporaryBest = toCheck;
					}
				}

				long long currentPairDistance = euclideanDistanceSquare(nodes[nodeIndex], nodes[nodeIndex + distance]);
				bestResult[nodeIndex][nodeIndex + distance] = currentPairDistance + temporaryBest;
			}
		}

		cout << bestResult[0][numberOfNodes - 1] - euclideanDistanceSquare(nodes[0], nodes[numberOfNodes - 1]);
		printBestDiagonal(bestResult, 0, numberOfNodes - 1);
		cout << endl;

		delete[] nodes;
		deleteMatrix(bestResult, numberOfNodes);
	}
}