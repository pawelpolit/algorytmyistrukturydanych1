/*
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

void initGraphs(vector<vector<int>>& p_graph, vector<vector<int>>& p_reversedGraph) {
    int numberOfEdges;
    cin >> numberOfEdges;

    while(numberOfEdges--) {
        int begin, end;
        cin >> begin >> end;
        p_graph[begin].push_back(end);
        p_reversedGraph[end].push_back(begin);
    }
}

void topoSort(vector<vector<int>>& p_graph, vector<vector<int>>& p_reversedGraph, vector<int>& p_result) {
    queue<int> queue;
    vector<int> numberOfNeighbours(p_graph.size());

    for(int i = 1; i < p_reversedGraph.size(); i++) {
        numberOfNeighbours[i] = p_reversedGraph[i].size();
        if(p_reversedGraph[i].empty()) {
            queue.push(i);
            numberOfNeighbours[i]--;
        }
    }

    while(!queue.empty()) {
        int neighbour = queue.front();
        queue.pop();
        p_result.push_back(neighbour);

        for(int i = 0; i < p_graph[neighbour].size(); i++) {
            if(--numberOfNeighbours[p_graph[neighbour][i]] == 0) {
                queue.push(p_graph[neighbour][i]);
            }
        }
    }
}

void computeLongestWay(vector<vector<int>>& p_reversedGraph, vector<int>& p_topoSortGraph, bool p_reversedTopoSort,
                       vector<int>& p_result) {

    int i = 0;
    int endCondition = p_topoSortGraph.size();
    int increment = 1;

    if(p_reversedTopoSort) {
        i = p_topoSortGraph.size() - 1;
        endCondition = -1;
        increment = -1;
    }

    for(; i != endCondition; i += increment) {
        if(p_reversedGraph[p_topoSortGraph[i]].empty()) {
            p_result[p_topoSortGraph[i]] = 1;
        } else {
            int max = 0;
            for(int j = 0; j < p_reversedGraph[p_topoSortGraph[i]].size(); j++) {
                if(p_result[p_reversedGraph[p_topoSortGraph[i]][j]] > max) {
                    max = p_result[p_reversedGraph[p_topoSortGraph[i]][j]];
                }
            }
            p_result[p_topoSortGraph[i]] = max + 1;
        }
    }
}

void printResult(vector<int>& p_longestWayTo, vector<int>& p_longestWayFrom) {
    for(int i = 1; i < p_longestWayTo.size(); i++) {
        cout << p_longestWayTo[i] + p_longestWayFrom[i] - 1 << ' ';
    }
    cout << endl;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfRouters;
        cin >> numberOfRouters;

        vector<vector<int>> graph(numberOfRouters + 1);
        vector<vector<int>> reversedGraph(numberOfRouters + 1);
        initGraphs(graph, reversedGraph);

        vector<int> topoSortGraph;
        topoSort(graph, reversedGraph, topoSortGraph);

        vector<int> longestWayTo(numberOfRouters + 1);
        computeLongestWay(reversedGraph, topoSortGraph, false, longestWayTo);

        vector<int> longestWayFrom(numberOfRouters + 1);
        computeLongestWay(graph, topoSortGraph, true, longestWayFrom);

        printResult(longestWayTo, longestWayFrom);
    }
}