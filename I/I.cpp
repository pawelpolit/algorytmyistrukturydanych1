/*
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>

using namespace std;

void prepareGraph(vector<vector<int>>& p_graph) {
    for(int i = 0; i < p_graph.size(); i++) {
        int numberOfNeighbours;
        cin >> numberOfNeighbours;

        while(numberOfNeighbours--) {
            int next;
            cin >> next;

            p_graph[i].push_back(next);
        }
    }
}

bool checkIfIsWinning(vector<int>& p_neighbours, vector<bool>& p_isWinning) {
    for(int neighbour : p_neighbours) {
        if(!p_isWinning[neighbour]) {
            return true;
        }
    }
    return false;
}

void printResult(vector<bool>& p_isWinning) {
    for(int win : p_isWinning) {
        cout << ((win) ? 'W' : 'P');
    }
    cout << endl;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfNodes;
        cin >> numberOfNodes;

        vector<vector<int>> graph(numberOfNodes);
        prepareGraph(graph);

        vector<bool> isWinning(numberOfNodes);

        for(int node = numberOfNodes - 1; node >= 0; node--) {
            isWinning[node] = checkIfIsWinning(graph[node], isWinning);
        }

        printResult(isWinning);
    }
}