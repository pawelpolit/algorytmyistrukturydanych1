/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class interval {
    int left, right;
    int numberOfPictures;
    int picturesBeforeInterval;

public:

    void setRange(int p_left, int p_right) {
        left = p_left;
        right = p_right;
    }

    void setNumberOfPictures(int p_numberOfPictures) {
        numberOfPictures = p_numberOfPictures;
    }

    int requiredNumberOfPictures(int p_allTakenPictures) {
        int result = numberOfPictures - (p_allTakenPictures - picturesBeforeInterval);
        return (result > 0) ? result : 0;
    }

    void setPicturesBeforeInterval(int p_picturesBeforeInterval) {
        picturesBeforeInterval = p_picturesBeforeInterval;
    }

    int begin() {
        return left;
    }

    int end() {
        return right;
    }
};

void loadData(vector<interval>& p_intervals) {
    for(int i = 0; i < p_intervals.size(); ++i) {
        int left, right, numberOfPictures;
        cin >> left >> right >> numberOfPictures;
        p_intervals[i].setRange(left, right);
        p_intervals[i].setNumberOfPictures(numberOfPictures);
    }
}

void fillWithIndices(vector<int>& p_vector) {
    for(int i = 0; i < p_vector.size(); ++i) {
        p_vector[i] = i;
    }
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfIntervals;
        cin >> numberOfIntervals;

        vector<interval> intervals(numberOfIntervals);
        loadData(intervals);

        vector<int> sortedByBegins(numberOfIntervals);
        fillWithIndices(sortedByBegins);
        sort(sortedByBegins.begin(), sortedByBegins.end(), [&intervals](int p_first, int p_second) {
            return intervals[p_first].begin() < intervals[p_second].begin();
        });

        vector<int> sortedByEnds(numberOfIntervals);
        fillWithIndices(sortedByEnds);
        sort(sortedByEnds.begin(), sortedByEnds.end(), [&intervals](int p_first, int p_second) {
            return intervals[p_first].end() < intervals[p_second].end();
        });

        int allTakenPictures = 0;
        vector<int>::iterator beginsIterator = sortedByBegins.begin();
        vector<int>::iterator endsIterator = sortedByEnds.begin();

        while(beginsIterator != sortedByBegins.end()) {
            if(intervals[*beginsIterator].begin() <= intervals[*endsIterator].end()) {
                intervals[*beginsIterator].setPicturesBeforeInterval(allTakenPictures);
                ++beginsIterator;
            } else {
                allTakenPictures += intervals[*endsIterator].requiredNumberOfPictures(allTakenPictures);
                ++endsIterator;
            }
        }

        while(endsIterator != sortedByEnds.end()) {
            allTakenPictures += intervals[*endsIterator].requiredNumberOfPictures(allTakenPictures);
            ++endsIterator;
        }

        cout << allTakenPictures << endl;
    }
}