/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>

using namespace std;

vector<int>* graph;
vector<int>* transposed;
int* order;
int* scc;
bool* visited;
int number, numberOfVariables;


void initGlobalVariables() {
    graph = new vector<int>[numberOfVariables + 1];
    transposed = new vector<int>[numberOfVariables + 1];
    order = new int[numberOfVariables];
    scc = new int[numberOfVariables + 1];
    visited = new bool[numberOfVariables + 1];
}

void deleteGlobalVariables() {
    delete[] graph;
    delete[] transposed;
    delete[] order;
    delete[] scc;
    delete[] visited;
}

void resetVisited() {
    for(int i = 0; i <= numberOfVariables; i++) {
        visited[i] = false;
    }
}

int negation(int p_variable) {
    if(p_variable <= numberOfVariables >> 1) {
        return p_variable + (numberOfVariables >> 1);
    } else {
        return p_variable - (numberOfVariables >> 1);
    }
}

void loadClauses(int p_numberOfClauses) {
    while(p_numberOfClauses--) {
        char firstNegation, secondNegation;
        int firstVariable, secondVariable;
        cin >> firstNegation >> firstVariable >> secondNegation >> secondVariable;

        if(firstNegation == '-') {
            firstVariable = negation(firstVariable);
        }

        if(secondNegation == '-') {
            secondVariable = negation(secondVariable);
        }

        graph[negation(firstVariable)].push_back(secondVariable);
        graph[negation(secondVariable)].push_back(firstVariable);
        transposed[secondVariable].push_back(negation(firstVariable));
        transposed[firstVariable].push_back(negation(secondVariable));
    }
}

void dfs(int x) {
    if(visited[x]) {
        return;
    }
    visited[x] = true;

    for(int i = 0; i < graph[x].size(); i++) {
        dfs(graph[x][i]);
    }

    order[number++] = x;
}


void dfs2(int x) {
    if(visited[x]) {
        return;
    }
    visited[x] = true;

    for(int i = 0; i < transposed[x].size(); i++) {
        dfs2(transposed[x][i]);
    }

    scc[x] = number;
}

void prepareScc(vector<int>& p_sccRepresentative) {
    number = 0;
    for(int i = 1; i <= numberOfVariables; i++) {
        if(!visited[i]) {
            dfs(i);
        }
    }

    number = 0;
    resetVisited();

    for(int i = numberOfVariables - 1; i >= 0; i--) {
        if(!visited[order[i]]) {
            dfs2(order[i]);
            p_sccRepresentative.push_back(order[i]);
            number++;
        }
    }
}

bool thereIsValuation() {
    for(int i = 1; i <= numberOfVariables >> 1; ++i) {
        if(scc[i] == scc[i + (numberOfVariables >> 1)]) {
            return false;
        }
    }
    return true;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfClauses;
        cin >> numberOfVariables >> numberOfClauses;
        numberOfVariables <<= 1;

        initGlobalVariables();
        resetVisited();
        loadClauses(numberOfClauses);

        vector<int> sccRepresentative;
        prepareScc(sccRepresentative);

        if(!thereIsValuation()) {
            cout << "NIE" << endl;
        } else {
            vector<int> sccValue(sccRepresentative.size(), -1);

            for(int i = sccRepresentative.size() - 1; i >= 0; --i) {
                if(sccValue[i] == -1) {
                    sccValue[i] = 1;
                    sccValue[scc[negation(sccRepresentative[i])]] = 0;
                }
            }

            cout << "TAK" << endl;

            for(int i = 1; i <= numberOfVariables >> 1; ++i) {
                cout << sccValue[scc[i]] << ' ';
            }
            cout << endl;
        }

        deleteGlobalVariables();
    }
}