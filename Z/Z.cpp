/**
 * Created by Pawel Polit
 */

#include <iostream>
#include <vector>
#include <unordered_map>
#include <limits>
#include <iomanip>

#define NOT_FOUND -1

using namespace std;


struct edge {
    int target, weight;

    edge(int p_target, int p_weight) : target(p_target), weight(p_weight) {}
};


void loadWordsGraph(vector<vector<edge>>& p_wordsGraph, int p_numberOfWords) {
    unordered_map<string, int> prefixToIndex;
    int indexForNewPrefix = 1;

    for(int i = 0; i < p_numberOfWords; ++i) {
        string word;
        cin >> word;

        string prefix = word.substr(0, 2);
        string sufix = word.substr(word.length() - 2, 2);

        int prefixIndex, sufixIndex;

        if(prefixToIndex.find(prefix) == prefixToIndex.end()) {
            prefixIndex = indexForNewPrefix++;
            prefixToIndex[prefix] = prefixIndex;
            p_wordsGraph.push_back(vector<edge>());
        } else {
            prefixIndex = prefixToIndex[prefix];
        }

        if(prefixToIndex.find(sufix) == prefixToIndex.end()) {
            sufixIndex = indexForNewPrefix++;
            prefixToIndex[sufix] = sufixIndex;
            p_wordsGraph.push_back(vector<edge>());
        } else {
            sufixIndex = prefixToIndex[sufix];
        }

        p_wordsGraph[prefixIndex].push_back(edge(sufixIndex, word.length()));
    }

    for(int i = 1; i < p_wordsGraph.size(); ++i) {
        p_wordsGraph[0].push_back(edge(i, 0));
    }
}


void relax(int p_begin, int p_end, double p_weight, vector<double>& p_shortestPathFromSource) {
    if(p_shortestPathFromSource[p_end] > p_shortestPathFromSource[p_begin] + p_weight) {

        p_shortestPathFromSource[p_end] = p_shortestPathFromSource[p_begin] + p_weight;
    }
}


bool hasPositiveCycle(vector<vector<edge>>& p_wordsGraph, double p_meanToSubtract) {
    vector<double> shortestPathFromSource(p_wordsGraph.size(), numeric_limits<double>::infinity());
    shortestPathFromSource[0] = 0;

    for(int iterationCounter = 0; iterationCounter < p_wordsGraph.size() - 1; ++iterationCounter) {

        for(int node = 0; node < p_wordsGraph.size(); ++node) {
            for(edge& neighbour : p_wordsGraph[node]) {
                relax(node, neighbour.target, p_meanToSubtract - neighbour.weight, shortestPathFromSource);
            }
        }
    }

    for(int node = 0; node < p_wordsGraph.size(); ++node) {
        for(edge& neighbour : p_wordsGraph[node]) {
            double oldValue = shortestPathFromSource[neighbour.target];
            relax(node, neighbour.target, p_meanToSubtract - neighbour.weight, shortestPathFromSource);

            if(oldValue != shortestPathFromSource[neighbour.target]) {
                return true;
            }
        }
    }

    return false;
}


double maxMeanOfWordLengthInGame(vector<vector<edge>>& p_wordsGraph) {
    double begin = 0, end = 1000, middle;

    while(end - begin >= 0.00001) {
        middle = (begin + end) / 2;

        if(hasPositiveCycle(p_wordsGraph, middle)) {
            begin = middle;
        } else {
            end = middle;
        }

        if(end < 2) {
            return NOT_FOUND;
        }
    }

    return (begin + end) / 2;
}


int main() {
    ios_base::sync_with_stdio(false);

    int z;
    cin >> z;

    while(z--) {
        int numberOfWords;
        cin >> numberOfWords;

        vector<vector<edge>> wordsGraph(1);
        loadWordsGraph(wordsGraph, numberOfWords);

        double bestMean = maxMeanOfWordLengthInGame(wordsGraph);

        if(bestMean == NOT_FOUND) {
            cout << "NIE" << endl;
        } else {
            cout << fixed << setprecision(4) << bestMean << endl;
        }
    }
}